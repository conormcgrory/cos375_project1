#!/bin/bash
# test_sim.sh 
# 
# This script tests the simulator on a number of binary files and 
# prints the outputs for each.
# 
# Author: Conor McGrory
# Date: 9 November 2015

echo "Test: simple1"
sim extra_tests/simple1/simple1.bin

echo "Test: add"
sim extra_tests/add/add.bin