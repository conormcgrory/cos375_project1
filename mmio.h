#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <xcb/xcb.h>

struct monitor {
  xcb_connection_t* connection;
  xcb_screen_t* screen;
  xcb_drawable_t window;
  xcb_gcontext_t FG[256];
};


struct monitor Initialize_Monitor(unsigned char* Memory) {
  int i;
  struct monitor moni;

  // initialize memory 
  for( i = 0xc000; i < 0xffff; i++) {
    *(Memory+i) = 255;
  }

  /* Open the connection to the X server */
  moni.connection = xcb_connect (NULL, NULL);
  /* Get the first screen */
  moni.screen = xcb_setup_roots_iterator (xcb_get_setup (moni.connection)).data;
  /* Create foreground graphic context */
  moni.window     = moni.screen->root;
  /* 256 different colors */
  //xcb_gcontext_t FG[256];
  uint32_t values[2];
  uint32_t mask = XCB_GC_FOREGROUND | XCB_GC_GRAPHICS_EXPOSURES;

  for(i = 0; i < 256; i++) {
    if (i == 255) {
      values[0] = 0xffffff;
    }
    else {
      values[0] = ((i & 0xe0)<<16) + ((i & 0x1c)<<11) + ((i & 0x3) << 6);
    }
    values[1] = 0;
    moni.FG[i] = xcb_generate_id (moni.connection);
    xcb_create_gc (moni.connection, moni.FG[i], moni.window, mask, values);
  }


  mask = XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK;
  values[0] = moni.screen->white_pixel;
  values[1] = XCB_EVENT_MASK_EXPOSURE;

  /* Create a window */
  moni.window = xcb_generate_id (moni.connection);

  xcb_create_window (moni.connection,                    /* connection          */
                     XCB_COPY_FROM_PARENT,          /* depth               */
                     moni.window,                        /* window Id           */
                     moni.screen->root,                  /* parent window       */
                     0, 0,                          /* x, y                */
                     256, 256,                      /* width, height       */
                     10,                            /* border_width        */
                     XCB_WINDOW_CLASS_INPUT_OUTPUT, /* class               */
                     moni.screen->root_visual,           /* visual              */
                     mask, values );                /* masks */

  /* Map the window on the screen and flush*/
  xcb_map_window (moni.connection, moni.window);
  xcb_flush (moni.connection);

  return moni;
}

void Update_Monitor(struct monitor moni, unsigned char* Memory) {
  int i;
  int color;
  int start_x,start_y;
  xcb_point_t point[] = {{0,0},{1,0},{0,1},{1,1}};
  for( i = 0xc000; i < 0xffff; i++) {
    start_x = ((i-0xc000)%128)*2;
    start_y = ((i-0xc000)/128)*2;
    point[0].x = start_x; point[0].y = start_y;
    point[1].x = start_x+1; point[1].y = start_y;
    point[2].x = start_x; point[2].y = start_y+1;
    point[3].x = start_x+1; point[3].y = start_y+1;
    color = (int)*(Memory+i);
    xcb_poly_point (moni.connection, XCB_COORD_MODE_ORIGIN, moni.window, moni.FG[color], 4, point);
    xcb_flush (moni.connection);
  }
  xcb_flush (moni.connection);
}

char Read_Keyboard(){
  char c = getchar();
  getchar();
  return c;
}
