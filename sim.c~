// COS 375: PAW Instruction Set
// Group: Terrence Kuo, Haley Chow, Natalia Perina, Conor McGrory
// Compile: gcc -g -o sim sim.c -lxcb
// Run: ./sim <some binary file>

#include <stdio.h>
#include "mmio.h"

/* PC is register 15, and FLAGS is register 16 */
enum {PC = 15, FLAGS = 16}; 

void formatZero(unsigned char OP, unsigned int* GeneralRegs);

void formatThree(unsigned char OP, unsigned char Rd, unsigned char Imm8, 
                 unsigned int* GeneralRegs);

void formatFour(unsigned char OP, unsigned char Rs, unsigned char Rd, 
                unsigned int* GeneralRegs);

void formatFive(unsigned char OP, unsigned char H1, unsigned char H2, 
                unsigned char Rs, unsigned char Rd, unsigned int* GeneralRegs);

void formatNine(unsigned char B, unsigned char L, unsigned char Imm5,
                unsigned char Rb, unsigned char Rd, unsigned int* GeneralRegs,
                unsigned char* Memory);

void formatTwelve(unsigned char SP, unsigned char Rd, unsigned char Imm8,
      unsigned int* GeneralRegs);

void formatSixteen(unsigned char Cond, char Offset, unsigned int* GeneralRegs);

void setStatusReg(unsigned int val, unsigned int* GeneralRegs);

void setCFlag(unsigned char Rs, unsigned char Rd, unsigned char Imm8, 
                  unsigned char op, unsigned int* GeneralRegs);

void setVFlag(unsigned char Rs, unsigned char Rd, unsigned char Imm8, 
                unsigned char add, unsigned int* GeneralRegs);


int main(int argc, char *argv[]){
  
    unsigned int * GeneralRegs; // 17 general registers
    GeneralRegs = (int *) malloc(17*sizeof(int));
    
    unsigned char *Memory; // PAW architecure uses 2^16 8-bit
    Memory = (char *) malloc(65536*sizeof(char)); 
    
    //struct monitor mn = Initialize_Monitor(Memory);

    FILE *fr; // file pointer
    int numOfBytes;

    if (sizeof(argv) != 2){
        fr = fopen(argv[1], "r"); // open binary file
        if (fr) // file has been read in correctly
        {
            numOfBytes = 0;
            while (!feof(fr)) // while not EOF
            {
                fread(&Memory[numOfBytes], 1, 1, fr); //read byte by byte
                //printf("%x\n", Memory[numOfBytes]); // print each byte stored
                numOfBytes++;
            }
        }
        fclose(fr);
    }
    
    GeneralRegs[PC] = 0; // set PC to first instruction

    int instructEncoding;
    while(1){
      // Update_Monitor(mn, Memory); // update monitor

      // corner case: check for 16 bit instruction
      if (GeneralRegs[PC]+1 >= numOfBytes)
        exit(0);

      // concat the two bytes in Memory to form an instruction set
      unsigned char lowerByte = Memory[GeneralRegs[PC]];
      unsigned char upperByte = Memory[GeneralRegs[PC]+1];
      int instruction = lowerByte | (upperByte << 8);
      printf("instruction hex: %x\n", instruction);
      
      // determine Instruction Set Encoding
      if ((upperByte >> 5) == 0)
        instructEncoding = 0;
      if ((upperByte >> 5) == 1)
        instructEncoding = 3;
      if ((upperByte >> 2) == 16)
        instructEncoding = 4;
      if ((upperByte >> 2) == 17)
        instructEncoding = 5;
      if ((upperByte >> 5) == 3)
        instructEncoding = 9;
      if ((upperByte >> 4) == 10)
        instructEncoding = 12;
      if ((upperByte >> 4) == 13)
        instructEncoding = 16;

      /*
        Based on the type of Instruction Encoding,
        a corresponding function will be called. 
        The corresponding function will determine the instruction type
      */
      switch (instructEncoding) {
         unsigned char OP, Rb, Rd, Rs, Imm8, Imm5, H1, H2, B, L, SP, cond, offset;

         case 0:
            OP = (upperByte & 0x1f);
            formatZero(OP, GeneralRegs);
            break;
         case 3:
            OP = (upperByte & 0x18) >> 3;
            Rd = (upperByte & 0x7);
            Imm8 = (lowerByte);
            printf("Before OP:%d Rd:%d Imm8:%d\n", OP, Rd, Imm8);
            formatThree(OP, Rd, Imm8, GeneralRegs);
            break;
         case 4:
            OP = (instruction & 0x3c0) >> 6; // b1111000000
            Rs = (instruction & 0x28) >> 3; // b111000
            Rd = (instruction & 0x7); // b111 
            formatFour(OP, Rs, Rd, GeneralRegs);
            break;
         case 5:
            OP = (upperByte & 0x3); // b11
            H1 = (lowerByte & 0x80) >> 7; // b10000000
            H2 = (lowerByte & 0x40) >> 6; // b1000000
            Rs = (instruction & 0x28) >> 3; // b111000
            Rd = (instruction & 0x7); // b111 
            formatFive(OP, H1, H2, Rs, Rd, GeneralRegs);
            break;
         case 9:
            B = (instruction & 0x1000) >> 12; // b1000000000000
            L = (instruction & 0x800) >> 11; // b100000000000
            Imm5 = (instruction & 0x7C0) >> 6; // b11111000000
            Rb = (instruction & 0x28) >> 3; // b111000
            Rd = (instruction & 0x7); //b111
            formatNine(B, L, Imm5, Rb, Rd, GeneralRegs, Memory);
            break;
         case 12:
            SP = (instruction & 0x800) >> 11; // b100000000000
            Rd = (instruction & 0x700) >> 8; //b 11100000000
            Imm8 = (instruction & 0xFF); // b11111111
            formatTwelve(SP, Rd, Imm8, GeneralRegs);
            break;
         case 16:
            cond = (instruction & 0xF00) >> 8; // b111100000000
            offset = (instruction & 0xFF); //b11111111
            formatSixteen(cond, offset, GeneralRegs);
            break;
         default:
            fprintf(stderr, "Illegal operation in instructEncoding\n");
            exit(1);
      }
    }
    
    free(Memory);
    free(GeneralRegs);
    
    return 0;
}

/*
  Below are the corresponding functions to determine
  the instruction type
*/
void formatZero(unsigned char OP, unsigned int* GeneralRegs){
  unsigned char instructType = OP;
  switch(instructType){
      int i;
      case 0: // HALT
        for(i = 0; i<17; i++)
          printf("Reg %d: %d\n",i, GeneralRegs[i]);
        printf("HALT\n");
        exit(0);
        break;
      default:
        fprintf(stderr, "Illegal operation in formatZero\n");
        exit(1);
        break;
  }
}

void formatThree(unsigned char OP, unsigned char Rd, unsigned char Imm8, 
                 unsigned int* GeneralRegs){
  unsigned char instructType = OP;
  unsigned int alu_out;
  printf("Format Type formatThree: %d\n", instructType);
  switch(instructType){
    case 0: // MOV IMMEDIATE
      GeneralRegs[Rd] = Imm8;
      setStatusReg(GeneralRegs[Rd], GeneralRegs);
      break;
    case 1: // CMP IMMEDIATE
      alu_out = GeneralRegs[Rd] - Imm8;
      setStatusReg(alu_out, GeneralRegs);
      break;
    default:
      fprintf(stderr, "Illegal operation in formatThree\n");
      exit(1);
  }

  GeneralRegs[PC] = GeneralRegs[PC] + 2;
}

void formatFour(unsigned char OP, unsigned char Rs, unsigned char Rd, unsigned int* GeneralRegs){
  unsigned char instructType = OP;
  unsigned char alu_out; 
  switch(instructType){
    case 0: // AND
      // Bitwise and 
      GeneralRegs[Rd] = GeneralRegs[Rd] & GeneralRegs[Rs];
      setStatusReg(GeneralRegs[Rd], GeneralRegs); 
      break;

    case 1: // EOR
      GeneralRegs[Rd] = GeneralRegs[Rd] ^ GeneralRegs[Rs]; 
      setStatusReg(GeneralRegs[Rd], GeneralRegs);
      break;

    case 2: // ASR
      if (GeneralRegs[Rs] == 0)
        break; 
      // if lower rs[7:0] < 16 
      else if (GeneralRegs[Rs] & 0xFF < 16) {
        setCFlag(Rs, Rd, 0, 1, GeneralRegs);
        GeneralRegs[Rd] = GeneralRegs[Rd] >> GeneralRegs[Rs];
        setStatusReg(GeneralRegs[Rd], GeneralRegs);
      }
      //needs to be finsihed 
      else 
        // if Rd[15] == 0 then rd = 0 
        if (GeneralRegs[Rd] & (1 << 15) == 0)
          GeneralRegs[Rd] = 0; 
        else 
          GeneralRegs[Rd] = 0xffff; 
        setStatusReg(GeneralRegs[Rd], GeneralRegs);
      break; 
    case 3: // TST
      alu_out = GeneralRegs[Rd] ^ GeneralRegs[Rs];
      setStatusReg(alu_out, GeneralRegs); 
      break;
    case 4: // NEG
      setCFlag(Rs, 0, 0, 3, GeneralRegs);
      setVFlag(Rs, 0, 0, 3, GeneralRegs); 
      GeneralRegs[Rd] = 0 - GeneralRegs[Rs]; 
      setStatusReg(GeneralRegs[Rd], GeneralRegs);
      break;
    case 5: // CMP
      setCFlag(Rs, Rd, 0, 2, GeneralRegs);
      setVFlag(Rs, Rd, 0, 1, GeneralRegs); //cmp is one for vFlag 
      alu_out = GeneralRegs[Rd] - GeneralRegs[Rs]; 
      setStatusReg(alu_out,  GeneralRegs); 
      break;
    default: 
      fprintf(stderr, "Illegal operation in formatFour\n");
      exit(1);
  }

  GeneralRegs[PC] = GeneralRegs[PC] + 2;
}

void formatFive(unsigned char OP, unsigned char H1, unsigned char H2, 
                unsigned char Rs, unsigned char Rd, unsigned int* GeneralRegs)
{
  unsigned char instructType = OP;
  unsigned char alu_out; 
  switch(instructType){
    case 0: // ADD
      setCFlag(Rs, Rd, 0, OP, GeneralRegs); //need to call before
      setVFlag(Rs, Rd, 0, OP, GeneralRegs); 
      GeneralRegs[Rd] = GeneralRegs[Rd] + GeneralRegs[Rs]; 
      setStatusReg(GeneralRegs[Rd], GeneralRegs);
      break;
    case 1: // CMP
      setCFlag(Rs, Rd, 0, 2, GeneralRegs);
      setVFlag(Rs, Rd, 0, OP, GeneralRegs);
      alu_out = GeneralRegs[Rd] - GeneralRegs[Rs];
      setStatusReg(alu_out, GeneralRegs); 
      break;
    case 2: // MOV
      GeneralRegs[Rd] = GeneralRegs[Rs];
      setStatusReg(GeneralRegs[Rd], GeneralRegs);
      break;
    default: 
      fprintf(stderr, "Illegal operation in formatFive\n");
      exit(1);
  }

  GeneralRegs[PC] = GeneralRegs[PC] + 2;
}

void formatNine(unsigned char B, unsigned char L, unsigned char Imm5, 
                unsigned char Rb, unsigned char Rd, unsigned int* GeneralRegs,
                unsigned char* Memory) 
{
  /* Use B and L to determine which instruction we have */
  unsigned char instructType = ((B && 0x01) << 2) && (L && 0x01);
  
  switch (instructType){
    unsigned int addr;
    unsigned char lo;
    unsigned char hi;
    
    case 0: // STR
      /* Compute address */
      addr = (Rb & 0xFFFC) + (Imm5 << 2);
      
      /* Extract low and high bytes from Rd */
      lo = GeneralRegs[Rd] && 0x00FF;
      hi = GeneralRegs[Rd] && 0xFF00;
      
      /* Store both bytes in memory (in order) */
      Memory[addr] = lo;
      Memory[addr + 1] = hi;
      break;
      
    case 1: // LDR
      /* Compute address */
      addr = (Rb & 0xFFFC) + (Imm5 << 2);

      /* Load low and high bytes from memory */
      lo = Memory[addr];
      hi = Memory[addr + 1];

      /* Combine bytes into halfword and store in Rd */
      GeneralRegs[Rd] = (hi << 8) | lo;
      break;

    case 2: //STRB
      /* Compute address */
      addr = Rb + Imm5;

      /* Store low byte of Rd in memory */
      Memory[addr] = GeneralRegs[Rd];
      break;

    case 3: //LDRB
      /* Compute address */
      addr = Rb + Imm5;

      /* Load byte at addr to register Rd */
      GeneralRegs[Rd] = Memory[addr];
      break;
  }

  GeneralRegs[PC] = GeneralRegs[PC] + 2;
}

void formatTwelve(unsigned char SP, unsigned char Rd, unsigned char Imm8,
      unsigned int* GeneralRegs)
{
  /* If SP != 0, then no instruction is specified */
  if (SP){
    fprintf(stderr, "Illegal operation in formatTwelve\n");
    exit(1);
  }

  /* If SP == 0, then instruction is ADD(2) */
  GeneralRegs[Rd] = (GeneralRegs[PC] & 0xFFFC) + (Imm8 << 2);
  
  GeneralRegs[PC] = GeneralRegs[PC] + 2;
}

void formatSixteen(unsigned char Cond, char Offset, unsigned int* GeneralRegs)
{
  unsigned char encoding = Cond;
  int SignExtend = (int) (Offset); 
  int flag = GeneralRegs[FLAGS];

  switch (encoding){ 

    // EQ, equal, Z = 1
    case 0:   
       if (flag & 0x4) //b01
       {
          GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
          return;
       }
       break; 

    // NE, not equal, Z = 0
     case 1:  
        if (!flag & 0x4)
        {
           GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
           return;
        }
      break; 

    // CS, carry set, unsigned higher or same, C = 1
    case 2:  
      if ((flag & 0x2))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 

    // CC, Carry clear/ unsigned lower, C = 0   
    case 3:  
      if (!(flag & 0x2))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 

    // MI, Minus/Negative, N = 1  
    case 4:  
      if (flag & 0x8) 
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 
    
    // PL, Plus/Positive or Zero, N = 0   
    case 5:  // PL, Plus/Positive or Zero, N = 0 
      if (!(flag & 0x8))
      {
         GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
         return;
      }
      break;

    // VS, overflow , V = 1/                                   
    case 6:  
      if (flag & 0x1)
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 

    // VC, no overflow, V =0 
    case 7: 
      if (!(flag & 0x8))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 

    // HI, unsigned higher, C = 1 && Z = 0 
    case 8: 
      if ((flag & 0x2) && (!(flag & 0x4)))
      {
         GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
         return;
      }
      break;

    // LS, unsigned lower or same, C = 0 || Z = 0 
    case 9: 
      if (!(flag & 0x2) || !(flag & 0x4))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 

    // GE, signed >=, N == V  
    case 10: // GE, signed >=, N == V
      if ((flag & 0x8) >> 3 == (flag & 0x1))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break;

    // LT, signed <, N != V    
    case 11: 
      if ((flag & 0x8) >> 3 != (flag & 0x1))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 
    
    // GT, signed >, Z = 0 && ( N == V) 
    case 12: 
      if (((flag & 0x8) >> 3 == (flag & 0x1)) && !(flag & 0x4))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break;

    // LE, signed <=, Z = 1 || (N != V)
    case 13: 
      if (((flag & 0x8) >> 3 != (flag & 0x1)) && (flag & 0x4))
      {
        GeneralRegs[PC] = GeneralRegs[PC] + SignExtend;
        return;
      }
      break; 
  }

  /* If we haven't returned by this point, all of the above if conditions 
   * have been false */
  GeneralRegs[PC] = GeneralRegs[PC] + 2;
}

// some instructions type do not need the cFlag or the vFlag
void setStatusReg(unsigned int val, unsigned int* GeneralRegs)
{
  unsigned char flag;
  // N Flag
  // obtain MSB
  
  flag = (val & 0x8000) >> 15; // b1000000000000000 
  GeneralRegs[16] = (GeneralRegs[16] | (flag << 3)); // place in index 3 

  // Z Flag
  if (val == 0)
    GeneralRegs[16] = GeneralRegs[16] | 0x4; // b0000000000000100
  else
    GeneralRegs[16] = GeneralRegs[16] & 0xFFFB; // b1111111111111011
}

void setCFlag(unsigned char Rs, unsigned char Rd, unsigned char Imm8,
        unsigned char op, unsigned int* GeneralRegs)
{
  unsigned long sum;
  unsigned char shiftAmt;
  unsigned char cFlag;
  unsigned int shiftedVal;
  
  if (op == 0) { //add
    sum = (unsigned long) GeneralRegs[Rs] + (unsigned long) GeneralRegs[Rd];
    cFlag = (sum & 0x0001FFFF) >> 17; //take the 17th bit as carry flag 
  }
    
  if (op == 1) { //shift
    shiftAmt = GeneralRegs[Rs];
    if (shiftAmt < 16) 
      shiftedVal = GeneralRegs[Rd] >> (GeneralRegs[Rs]-1);
    else 
      shiftedVal = GeneralRegs[Rd] >> 15;
    cFlag = shiftedVal & 0x1; //b0000000000000001
  }
  
  if (op == 2) { //compare
    if (Imm8 != 0)
      sum = (unsigned long) GeneralRegs[Rs] - (unsigned long) Imm8;
    else 
      sum = (unsigned long) GeneralRegs[Rs] - (unsigned long) GeneralRegs[Rd];
    cFlag = (sum & 0x0001FFFF) >> 17; 
  }

  if (op == 3) { //neg
    sum = (unsigned long) 0 - (unsigned long) GeneralRegs[Rd];
    cFlag = (sum & 0x0001FFFF) >> 17;
  }
 
  if (cFlag == 1)
    GeneralRegs[16] = GeneralRegs[16] | 0x2; //b0000000000000010
  else 
    GeneralRegs[16] = GeneralRegs[16] & 0xFFFD; //b1111111111111101

}

void setVFlag(unsigned char Rs, unsigned char Rd, unsigned char Imm8,
        unsigned char op, unsigned int* GeneralRegs)
{
    unsigned char vFlag = 0;
    unsigned char signBitOne;
    unsigned char signBitTwo;
    unsigned int sum;

    signBitOne = GeneralRegs[Rs] >> 15; //get the first bit of each value
    if (op == 0){ //add
      signBitTwo = GeneralRegs[Rd] >> 15;
      if (signBitOne == signBitTwo) { //check if sign bits are the same
        sum = GeneralRegs[Rs] + GeneralRegs[Rd];
        if (signBitOne != sum >> 15) //set vFlag if the sum's sign bit is different
          vFlag = 1;
      }
    }

    else if (op == 1) { //cmp, with/without immediate
      if (Imm8 == 0)
        signBitTwo = GeneralRegs[Rd] >> 15;
      else 
        signBitTwo = Imm8 >> 7; //immediate is 8 bits
      if (signBitOne == signBitTwo) { 
        sum = GeneralRegs[Rs] - GeneralRegs[Rd];
        if (signBitOne != sum >> 15) //set vFlag if the sum's sign bit is different
          vFlag = 1;
      }
    }

    else { //neg
      signBitTwo = 0;
      if (signBitOne == signBitTwo) { 
        sum = 0 - GeneralRegs[Rs];      
        if (signBitOne != sum >> 15) //set vFlag if the sum's sign bit is different
            vFlag = 1;
      }
    }

    if (vFlag == 1)
      GeneralRegs[16] = GeneralRegs[16] | 0x1; //b0000000000000001
    else 
      GeneralRegs[16] = GeneralRegs[16] & 0xFFFE; //b1111111111111110
  }
