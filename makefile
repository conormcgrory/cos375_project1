# Makefile for PAW simulator
# COS 375 Project 1
#
# Group: Conor McGrory, Terrence Kuo, Natalia Perina, Haley Chow
#
all: sim

sim: sim.c mmio.h
	gcc -g -o sim sim.c -lxcb