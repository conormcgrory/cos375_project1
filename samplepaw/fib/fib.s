@ fib.s - calculates Fibonacci numbers
@
@	r0 - loop counter (n)
@	r2 - Fib(n)
@	r3 - Fib(n-1)
@
@	r1, r4 - temporary registers
@
.text
init:
	mov r0, #2
	mov r2, #1
	mov r3, #0
	mov r4, #1

loop:
	mov r1, r2
	add r1, r3
	mov r3, r2
	mov r2, r1
	add r0, r4
	cmp r0, #10
	blt loop
end:	
	halt


