@strcopy.s - copy a string from one memory location to another
@ 
@	r0 - pointer to the source string
@	r1 - pointer to the destination string
@	

.text
	adr 	r0, .scrstr
	adr 	r1, .dststr
	mov 	r3, #1
strcopy:	
	ldrb	r2, [r0]
	strb	r2, [r1]
	add	r0, r3
	add	r1, r3
	cmp	r2, #0
	bne	strcopy
end:
	halt

.align 4
.scrstr:
	.string "This is my source string"

.align 4
.dststr:
	.string "This is my destination string"
