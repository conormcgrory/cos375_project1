@ bitstr.s - convert a bit string into an integer
@
@	r0 - integer value (output)
@	r1 - string (input)
@	r2 - current character
@	r3 - constant 1
@	r4 - end string
@	r5 - temporary result
.text
init:
	mov r0, #0
	mov r3, #1
	mov r5, #0
	adr r1, .mystr
	adr r4, .mystr + 16
loop:
	ldrb r2, [r1]
	cmp r2, #0
	beq end
	cmp r2, #48
	beq zero
	cmp r2, #49
	bne end
one:
	add r5, r3
zero:	
	mov r0, r5
	add r1, r3	
	add r5, r5
	cmp r1, r4
	bne loop
end:
	halt

.align 4
.mystr:
	.string "001101101"

