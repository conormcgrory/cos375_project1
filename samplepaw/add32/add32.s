@ add32.s - adds two 32-bit integers
@
@	r0, r1 - hi and lo result (output)
@	r2, r3 - ptr to 32-bit ints (input)
@	r3-r7  - temporaries

init:
	adr r2, .int1
	adr r3, .int2
	adr r6, .tmp
	mov r7, #1
	
main:
	ldr r1, [r2]
	ldr r4, [r3]
	
	ldrb r5, [r2, #2]
	strb r5, [r6, #0]
	ldrb r5, [r2, #3]
	strb r5, [r6, #1]
	ldr  r0, [r6]
	
	ldrb r5, [r3, #2]
	strb r5, [r6, #0]
	ldrb r5, [r3, #3]
	strb r5, [r6, #1]
	ldr  r5, [r6]

	add  r1, r4
	bcc  nocarry

carry:
	add r0, r7

nocarry:
	add r0, r5
	halt

.align 4
.int1:
	.word 65516

.align 4
.int2:
	.word 84

.align 4
.tmp:	
	.word 0

