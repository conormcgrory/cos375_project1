@ sum.s - calculates sum of 1 to 100
@
@       r3 - loop counter (n)
@       r4 - sum
@
@       r2 - temporary registers
@

.text
start:
	MOV r2, #1
	MOV r3, #1
	MOV r4, #0
start_loop:
	CMP r3, #100
	BGT end_loop
	ADD r4, r3
	ADD r3, r2
	BVC start_loop
end_loop:
	HALT
	