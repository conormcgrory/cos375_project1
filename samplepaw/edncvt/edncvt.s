@edncvt.s - convert a word stored as big endian to a word stored 
@	    as little endian or vice verse
@
@	r0 - pointer to the 32-bit integer (input)
@	r1 - pointer to the 32-bit integer (output)

.text
	adr r0, .input
	adr r1, .output
	mov r7,#1

	ldrb r5, [r0]
	strb r5, [r1, #3]
	ldrb r5, [r0, #1]
	strb r5, [r1, #2]
	ldrb r5, [r0, #2]
	strb r5, [r1, #1]
	ldrb r5, [r0, #3]
	strb r5, [r1]
	halt

.align 4
.input:
	.word 32268

.align 4
.output: 
	.word 0
