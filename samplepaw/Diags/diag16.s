@ This program uses multiply two unsigned 8 bit number

data:
	ADD r2,pc,#200
	LDRB r0,[r2,#0] @ load multiplicand
	LDRB r1,[r2,#1] @ load multiplier
	MOV r3,#1
	NEG r3,r3
	MOV r4,#8
	MOV r8,r4
	MOV r4,#0
	MOV r10,r4
loop:	
	ASR r0,r3
	BCC cc
	ADD r10,r0
cc:
	MOV r5,r10
	ADD r10,r5
	
	ADD r8,r3
	MOV r4,r8
	CMP r4,#0
	BNE loop
	
	MOV r5,r10
	STR r5,[r2,#4] @the result is stored
