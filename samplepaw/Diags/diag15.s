@ This example use shift to perform a mulpication with constant 1.4142, which if
@ expressed as binary fraction is 1.01101010

data:	
	ADD r0,PC,#100
	LDRB r5,[r0,#0]	@get the multiplicant  
begin:
	MOV r2,#2
	MOV r3,#3
	
	MOV r9,r5
	MOV r7,r9
	ASR r7,r2
	ADD r9,r7	
			@r9=r5+r5>>2 (r9=1.01*r5)
	MOV r7,r9
	ASR r7,r2
	MOV r8,r5
	ADD r8,r7
			@r8=r5+r9>>2 (r6=1.0101*r5)
	MOV r7,r8
	ASR r7,r3
	ADD r9,r7
			@r9=r9+r8>>3 (r9=1.0110101*r5)
end:
	MOV r1,r9
	STRB r1,[r0,#4]	@the result is stored
