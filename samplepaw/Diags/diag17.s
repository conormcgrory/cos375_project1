@This program is flip every bit of a 8 bit number

data:
	ADD r0,pc,#100
	LDRB r7,[r0,#0] @the number to be flipped is loaded
begin:
	MOV r1,#1
	MOV r9,r7

	MOV r3,#8
	MOV r11,r3
	NEG r4,r1
	MOV r5,#0
loop:
	TST r7,r1
	BEQ zero
one:
	NEG r2,r1
	ADD r9,r2
	BVC bitflipped
zero:	
	ADD r9,r1
bitflipped:
	MOV r10,r1
	ADD r1,r10
	ADD r11,r4
	MOV r3,r11
	EOR r3,r5
	BNE loop

	MOV r6,r9
	STRB r6,[r0,#1] @the number flipped is stored
	AND r7,r6
	BNE wrong
	STRB r5,[r0,#2] @store 0 if result is right
	BEQ end
wrong:
	MOV r5,#1
	STRB r5,[r0,#2] @store 1 if result is wrong
end:
