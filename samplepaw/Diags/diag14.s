@ This simple program calculate the absolute value of the difference of two numbers

data:
	ADD r4,pc,#100  
	LDR r0,[r4,#0]	@ one number is loaded
	LDR r1,[r4,#4]	@ the other number is loaded
begin:
	CMP r0,r1
	BEQ equal
	BLT less
	NEG r2,r1
	MOV r8,r2
	ADD r8,r0
	BVC str_result
	BVS overflow
equal:
	MOV r2,#0
	MOV r8,r2
	BVC str_result
less:
	NEG r2,r0
	MOV r8,r2
	ADD r8,r1
	BVS overflow
str_result:	
	MOV r2,r8
	STR r2,[r4,#8] @the absolute difference is stored
overflow:
	MOV r2,#1
	NEG r2,r2
	STR r2,[r4,#8] @if overflow, write -1 in it		
