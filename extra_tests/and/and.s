@ and.s - test AND instruction
@
@ At the end of the test, r0 should be equal to 4 and r1 should
@ be equal to 6. The computation being performed is the bitwise
@ AND operation:
@       5 (0b101) & 6(0b110) = 4(0b100)
.text
init:
        mov r0, #5
        mov r1, #6
        and r0, r1
end:
        halt
       