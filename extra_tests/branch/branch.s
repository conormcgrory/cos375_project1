@ branch.s - Tests the branch command b (for all possible conditions)
@
@ If r1 contains the number 3 at the end, at least one test was failed.
.text   
        
init:
        @ Initialize r0 to 1
        mov r0, #1      

        @ Test BEQ:
        cmp r0, #1
        beq pass1

        @ Failure 
        mov r1, #3

pass1:
        @ Test BNE:
        cmp r0, #0
        bne pass2

        @ Failure
        mov r1, #3
        
pass2:
        @ Test CS:
        mov r0, #255

        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        
        @ At this point r0 = 2^16 - 2^8. Multiplying by 2 again causes a carry
        add r0, r0

        @ Now the carry flag should be set to 1.
        bcs pass3

        @ Failure
        mov r1, #3

pass3:
        @ Test CC:
        mov r0, #10

        @ This addition (10 + 10 = 20) should not cause a carry.
        add r0, r0
        bcc pass4

        @ Failure
        mov r1, #3

pass4:  
        @ Test MI:
        mov r0, #1

        @ The subtraction implied by this CMP should set N to 1
        cmp r0, #2
        bmi pass5

        @ Failure
        mov r1, #3
        
pass5:
        @ Test PL:
        mov r0, #2

        @ The subtraction implied by this CMP should not set N to 1
        cmp r0, #1
        bpl pass6

        @ Failure
        mov r1, #3

pass6:
        @ Test VS:
        mov r0, #255

        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        add r0, r0
        
        @ At this point r0 = 2^16 - 2^8. Multiplying by 2 again causes overflow
        add r0, r0

        @ Now the overflow flag should be set to 1
        bvs pass7

        @ Failure
        mov r1, #3
        
pass7:  
        
end:
        halt
        