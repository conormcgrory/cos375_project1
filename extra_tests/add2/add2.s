@ add2.s - Test ADD(2) instruction
@
@ PC + 8 is stored in r0.
.text
init:
        add r0, r15, #8
end:
        halt
        
        