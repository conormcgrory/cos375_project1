@ strldr.s - Tests STR (store halfword) and LDR (load halfword) instructions
@
@ Store the immediate number 300 (which takes more than 8 bits) in the memory
@ address 20(base) + 4(offset) = 24, and then load it in to register r2.
@        
.text
init:
        mov r0, #150
        mov r1, #150
        add r0, r1
        
        mov r1, #20

        str r0, [r1,#4]
        ldr r2, [r1,#4]
end:
        halt


        