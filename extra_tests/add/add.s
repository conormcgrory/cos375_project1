@ add.s - Test the ADD(1) instruction
@
@ This test assumes that the MOV immediate and HALT instructions already work.
@ At the end of the test, r0 should contain the number 5, and r1 should contain
@ the number 3.
@
.text
init:
        mov r0, #2
        mov r1, #3
        mov r0, r1

end:
        halt
        