@ strbldrb.s - Tests STRB (store byte) and LDRB (load byte) instructions
@
@ Store the immediate number 30 at the memory address 20(bas) + 4(offset) = 24,
@ and then load it in to register r2.
@        
.text
init:
        mov r0, #30
        
        mov r1, #20

        strb r0, [r1, #4]
        ldrb r2, [r1, #4]
end:
        halt


        